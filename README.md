# Spring Test Fixtures

A description for object test fixtures to be used in spring tests.

## Key aspects

- a separate component for each entity type
- apply `@TestComponent` annotation
- mandatory parameter-less `create` method
  - enforce with typed interface
- optional parameters in different creation methods
- creation persists object using `TestEntityManager`
- may depend on other fixture components
- usage via `@Autowired` in integration tests; `@Import` may be needed
